/* $Id: namei.h,v 1.16 2000/04/13 00:55:54 davem Exp $
 * linux/include/asm-sparc/namei.h
 *
 * Routines to handle famous /usr/gnemul/s*.
 * Included from linux/fs/namei.c
 */

#ifndef __SPARC_NAMEI_H
#define __SPARC_NAMEI_H

#define __emul_prefix() NULL

#endif /* __SPARC_NAMEI_H */
